package com.devcamp.province.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.province.model.CProvince;
import com.devcamp.province.model.CDistrict;
import com.devcamp.province.model.CWard;
import com.devcamp.province.repository.IProvinceRepository;
import com.devcamp.province.repository.IDistrictRepository;
import com.devcamp.province.repository.IWardRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProviceController {
	@Autowired
    IProvinceRepository pProvinceRepository;
	
	@Autowired
    IDistrictRepository pDistrictRepository;

    	@Autowired
    IWardRepository pIWardRepository;
	
	@GetMapping("/provice")
	public ResponseEntity<List<CProvince>> getAllProvince() {
        try {
            List<CProvince> provinces = new ArrayList<CProvince>();

            pProvinceRepository.findAll().forEach(provinces::add);

            return new ResponseEntity<>(provinces, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GetMapping("/district")
	public ResponseEntity<Set<CDistrict>> getRegionsByCountryCode(@RequestParam String countryCode) {
        try {
            CProvince vProvince = pProvinceRepository.findByProvinceCode(countryCode);
            
            if(vProvince != null) {
            	return new ResponseEntity<>(vProvince.getDistricts(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    	@GetMapping("/ward")
	public ResponseEntity<Set<CWard>> getWardByDistrictCode(@RequestParam String districtCode) {
        try {
            CDistrict vDistrict = pDistrictRepository.findByDistrictPrefix(districtCode);
            
            if(vDistrict != null) {
            	return new ResponseEntity<>(vDistrict.getWards(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
