package com.devcamp.province.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.*;

@Entity
@Table(name = "province")
public class CProvince {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long provinceid;
	
	@Column(name = "province_code", unique = true)
	private String provinceCode;
	
	@Column(name = "province_name")
	private String provinceName;
	
	@OneToMany(mappedBy = "province", cascade = CascadeType.ALL)
    @JsonManagedReference
	private Set<CDistrict> district;

    public CProvince() {
    }

    public CProvince(String provinceCode, String provinceName) {
        this.provinceCode = provinceCode;
        this.provinceName = provinceName;
    }

    public long getProvince_id() {
        return provinceid;
    }

    public void setprovinceid(long provinceid) {
        this.provinceid = provinceid;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public Set<CDistrict> getDistricts() {
        return district;
    }

    public void setDistricts(Set<CDistrict> districts) {
        this.district = districts;
    }
    
}
