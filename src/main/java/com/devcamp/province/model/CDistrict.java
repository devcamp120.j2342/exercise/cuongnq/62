package com.devcamp.province.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.*;


@Entity
@Table(name = "district")
public class CDistrict {
    	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long distric_id;
	
	@Column(name = "district_prefix", unique = true)
	private String districtPrefix;
	
	@Column(name = "district_name")
	private String districtName;
	
	@ManyToOne
    @JoinColumn(name="provinceid")
    private CProvince province;


    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    @JsonManagedReference
	private Set<CWard> Wards;


	public CDistrict(String strictPrefix, String strictName) {
		this.districtPrefix = strictPrefix;
		this.districtName = strictName;
	}


	public CDistrict() {
	}


	public long getStrict_id() {
		return distric_id;
	}


	public void setdistric_id(long distric_id) {
		this.distric_id = distric_id;
	}


	public String getStrictPrefix() {
		return districtPrefix;
	}


	public void setStrictPrefix(String strictPrefix) {
		this.districtPrefix = strictPrefix;
	}


	public String getStrictName() {
		return districtName;
	}


	public void setStrictName(String strictName) {
		this.districtName = strictName;
	}


	public CProvince getProvince() {
		return province;
	}


	public void setProvince(CProvince Province) {
		province = Province;
	}


	public Set<CWard> getWards() {
		return Wards;
	}


	public void setWards(Set<CWard> wards) {
		Wards = wards;
	}

	
}
