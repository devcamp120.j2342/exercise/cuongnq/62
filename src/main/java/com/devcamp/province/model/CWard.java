package com.devcamp.province.model;



import jakarta.persistence.*;


@Entity
@Table(name = "ward")
public class CWard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long ward_id;
	
	@Column(name = "ward_prefix", unique = true)
	private String wardPrefix;
	
	@Column(name = "ward_name")
	private String wardName;
	
	@ManyToOne
    @JoinColumn(name="distric_id")
    private CDistrict district;



    public String getWardPrefix() {
        return wardPrefix;
    }

    public void setWardPrefix(String wardPrefix) {
        this.wardPrefix = wardPrefix;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public CWard(String wardPrefix, String wardName) {
        this.wardPrefix = wardPrefix;
        this.wardName = wardName;
    }

    public CWard() {
    }

    public long getWard_id() {
        return ward_id;
    }

    public void setWard_id(long ward_id) {
        this.ward_id = ward_id;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    
}
