package com.devcamp.province.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province.model.CWard;

public interface IWardRepository extends JpaRepository<CWard, Long> {

}