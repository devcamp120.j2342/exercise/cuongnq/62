package com.devcamp.province.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province.model.CProvince;

public interface IProvinceRepository extends JpaRepository<CProvince, Long> {
	CProvince findByProvinceCode(String provinceCode);
}
