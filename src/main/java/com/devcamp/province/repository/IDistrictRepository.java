package com.devcamp.province.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province.model.CDistrict;


public interface IDistrictRepository extends JpaRepository<CDistrict, Long> {
    CDistrict findByDistrictPrefix(String districtPrefix);
}
